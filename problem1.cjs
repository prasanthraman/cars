//finds the details of car_id
// pass inventory and car_id as arguements to this funtion

function problem1(inventory,car_id){
  
    let flag=false;
    if ((typeof(inventory) !='object') || !(Array.isArray(inventory)) || (typeof(car_id) !='number')){
        console.log("Incorrect data passed as arguements")
        return [];
    }
    for (let i=0 ;i<inventory.length;i++){
    
        if (inventory[i].id==car_id){
            flag=true;
            //console.log("Car %d is a %d %s %s.", car_id,inventory[i].car_year,inventory[i].car_make,inventory[i].car_model); 
            return [inventory[i]]
            //return ("Car " + car_id + " is a " + inventory[i].car_year+" "+inventory[i].car_make+" "+inventory[i].car_model + ".");
   
        };
    }

    if (flag==false){
        return [];
    }

};

module.exports = problem1;