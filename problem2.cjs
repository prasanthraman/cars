//finds last car and fetches its details in the inventory.
function problem2(inventory){
    if ((typeof(inventory) !='object') || !(Array.isArray(inventory))){
        console.log("Incorrect data passed as arguements")
        return [];
    }if( inventory.length==0) {
        return [];
    }
console.log("Last car is %s %s.", inventory[(inventory.length)-1].car_make,inventory[(inventory.length)-1].car_model)
return[inventory[(inventory.length)-1]]
};
module.exports = problem2;