// gets the array of car models and returns sorted array by its alphabetical order
function problem3(inventory){
    if ((typeof(inventory) !='object') || !(Array.isArray(inventory))){
        console.log("Incorrect data passed as arguements")
        return [];
    }if( inventory.length==0) {
        return [];
    }
    let car_models=[]
    for(let i=0; i<inventory.length;i++){
        car_models.push((inventory[i].car_model))
    }
    
    return car_models.sort();

}

module.exports = problem3;