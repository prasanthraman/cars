//returns an array of car_years
function problem4(inventory){
    if ((typeof(inventory) !='object') || !(Array.isArray(inventory))){
        console.log("Incorrect data passed as arguements")
        return [];
    }if( inventory.length==0) {
        return [];
    }
    let car_years=[]
    for (let i=0; i<inventory.length;i++){
        car_years.push(inventory[i].car_year)

    }
    return car_years
}
module.exports = problem4;
