//logs cars whose car make is either Audi or BMW
function problem6(inventory){
    let filtered_cars=[]
    if ((typeof(inventory) !='object') || !(Array.isArray(inventory))){
        console.log("Incorrect data passed as arguements")
        return [];
    }if( inventory.length==0) {
        return [];
    }
    for(let i=0;i<inventory.length;i++)
    {
        if(inventory[i].car_make=='Audi'||inventory[i].car_make=='BMW'){
            filtered_cars.push(JSON.stringify(inventory[i]))
        }
    }
    return filtered_cars;

}
module.exports = problem6;