let problem1=require("../problem1.cjs")
let _= require("../cars.js")
let inventory=_.inventory_list()

const result =problem1(123,123) // Call the function with inventory and Car_id as arguements
//console.log(result)
if(result.length==0){
    console.log("Empty Array")
    return;
}
console.log("Car %d is a %d %s %s.", result[0].id,result[0].car_year,result[0].car_make,result[0].car_model);